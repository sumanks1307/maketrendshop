<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\StaticPage;


class PageController extends Controller
{
    
    public function PagesIndex()

    {
        $pages = StaticPage::all();
        return view('admin.pages.index', compact('pages'));
    }

    public function pagecreate()
    {
        return view('admin.pages.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required|string|max:255',
            'slug' => 'required|string|max:255|unique:static_pages,slug',
            'content' => 'required',
        ]);

        StaticPage::create($request->all());

        return redirect()->route('admin.pages.index')->with('success', 'Page created successfully.');
    }

    public function edit(StaticPage $staticPage)
    {
        return view('admin.pages.edit', compact('staticPage'));
    }


    public function update(Request $request, StaticPage $staticPage)
{
    $request->validate([
        'title' => 'required|string|max:255',
        'slug' => 'required|string|max:255|unique:static_pages,slug,' . $staticPage->id,
        'content' => 'required',
    ]);

    $staticPage->update($request->only(['title', 'slug', 'content']));

    return redirect()->route('admin.pages.index')->with('success', 'Page updated successfully.');
}

public function destroy(StaticPage $staticPage)
{
    $staticPage->delete();

    return redirect()->route('admin.pages.index')->with('success', 'Page deleted successfully.');
}




}
