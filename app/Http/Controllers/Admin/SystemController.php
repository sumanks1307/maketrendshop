<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class SystemController extends Controller
{
    
    public function index()
    {
        $customCss = Storage::disk('public')->exists('custom.css') ? Storage::disk('public')->get('custom.css') : '';

        return view('admin.customcss.indexcss', compact('customCss'));
    }

    public function saveCss(Request $request)
    {
        $request->validate([
            'custom_css' => 'required|string',
        ]);

        Storage::disk('public')->put('custom.css', $request->input('custom_css'));

        return redirect()->back()->with('success', 'Custom CSS saved successfully!');
    }

}
