@extends('front.layouts.base')
@section('content')

@include('front.sections.banner')


@include('front.sections.ads')


@include('front.sections.NewArrival')



@include('front.sections.OurCategory')



@include('front.sections.PopularCollection')


@include('front.sections.TopDeals')

@include('front.sections.NewsLetter')

@endsection