<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Models\BannerImage;
use App\Models\Banner;

class BannerController extends Controller
{
    
    public function index()
    {
        $banners = Banner::all();
        return view('admin.banners.index', compact('banners'));
    }


    public function create()
    {
        return view('admin.banners.create');
    }


    public function store(Request $request)
    {
        $validated = $request->validate([
            'main_image.*' => 'required|image',
            'thumbnail_image.*' => 'required|image',
            'sale_title' => 'required|string|max:255',
            'product_title' => 'required|string|max:255',
            'promotion' => 'required|string|max:255',
            'new_price' => 'required|numeric',
            'old_price' => 'required|numeric',
            'description' => 'required|string',
        ]);
    
        $banner = new Banner([
            'sale_title' => $request->sale_title,
            'product_title' => $request->product_title,
            'promotion' => $request->promotion,
            'new_price' => $request->new_price,
            'old_price' => $request->old_price,
            'description' => $request->description,
        ]);
    
        $banner->save();
    
        foreach ($request->file('main_image') as $mainImage) {
            $path = $mainImage->store('banners/main', 'public');
            $banner->images()->create(['path' => $path, 'type' => 'main']);
        }
    
        foreach ($request->file('thumbnail_image') as $thumbnailImage) {
            $path = $thumbnailImage->store('banners/thumbnail', 'public');
            $banner->images()->create(['path' => $path, 'type' => 'thumbnail']);
        }
    
        return redirect()->route('admin.banner.index')->with('success', 'Banner created successfully.');
    }
    

    public function edit($id)
    {
        $banner = Banner::findOrFail($id);
        return view('admin.banners.edit', compact('banner'));
    }


    public function update(Request $request, $id)
    {
        $validated = $request->validate([
            'main_image.*' => 'image',
            'thumbnail_image.*' => 'image',
            'sale_title' => 'required|string|max:255',
            'product_title' => 'required|string|max:255',
            'promotion' => 'required|string|max:255',
            'new_price' => 'required|numeric',
            'old_price' => 'required|numeric',
            'description' => 'required|string',
        ]);
    
        $banner = Banner::findOrFail($id);
    
        if ($request->hasFile('main_image')) {
            // Delete old images
            foreach ($banner->images->where('type', 'main') as $oldImage) {
                Storage::disk('public')->delete($oldImage->path);
                $oldImage->delete();
            }
    
            // Upload new images
            foreach ($request->file('main_image') as $mainImage) {
                $path = $mainImage->store('banners/main', 'public');
                BannerImage::create([
                    'banner_id' => $banner->id,
                    'path' => $path,
                    'type' => 'main',
                ]);
            }
        }
    
        if ($request->hasFile('thumbnail_image')) {
            // Delete old images
            foreach ($banner->images->where('type', 'thumbnail') as $oldImage) {
                Storage::disk('public')->delete($oldImage->path);
                $oldImage->delete();
            }
    
            // Upload new images
            foreach ($request->file('thumbnail_image') as $thumbnailImage) {
                $path = $thumbnailImage->store('banners/main', 'public');
                BannerImage::create([
                    'banner_id' => $banner->id,
                    'path' => $path,
                    'type' => 'thumbnail',
                ]);
            }
        }
    
        // Update banner details
        $banner->sale_title = $request->sale_title;
        $banner->product_title = $request->product_title;
        $banner->promotion = $request->promotion;
        $banner->new_price = $request->new_price;
        $banner->old_price = $request->old_price;
        $banner->description = $request->description;
        $banner->save();
    
        return redirect()->route('admin.banner.index')->with('success', 'Banner updated successfully.');
    }
    
    
    public function delete($id)
    {
        $banner = Banner::findOrFail($id);
        $banner->delete();

        return redirect()->route('admin.banner.index')->with('success', 'Banner deleted successfully.');
    }
    

}
