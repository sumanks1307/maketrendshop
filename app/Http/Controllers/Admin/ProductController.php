<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Category;
use App\Models\Brand;
use App\Models\Discount;

class ProductController extends Controller
{
    public function productindex(Request $request)
    {
        $products = Product::all();
        return view('admin.products.index',  compact('products')); 

    }

    public function create()
    {
        $categories = Category::all();
        $brands = Brand::all(); 
        $discounts = Discount::all();
        return view('admin.products.create', compact('categories', 'brands','discounts'));
    }

 

public function store(Request $request)
{
    // Validate request
    $request->validate([
        'name' => 'required',
        'slug' => 'required|unique:products',
        'description' => 'required',
        'regular_price' => 'required|numeric|min:0',
        'sale_price' => 'nullable|numeric|min:0',
        'SKU' => 'nullable|unique:products',
        'stock_status' => 'required|in:instock,outofstock',
        'featured' => 'boolean',
        'quantity' => 'required|integer|min:0',
        'image' => 'nullable|image|mimes:jpeg,png,jpg,gif|max:2048', // Adjust the max file size as needed
        'category_id' => 'required|exists:categories,id',
        'brand_id' => 'required|exists:brands,id',
        'discount_id' => 'exists:discounts,id',
    ]);

    // Handle image upload
    if ($request->hasFile('image')) {
        $imagePath = $request->file('image')->store('products', 'public');
    } else {
        $imagePath = null;
    }

    // Create new product
    $product = new Product();
    $product->name = $request->input('name');
    $product->slug = $request->input('slug');
    $product->description = $request->input('description');
    $product->regular_price = $request->input('regular_price');
    $product->sale_price = $request->input('sale_price');
    $product->SKU = $request->input('SKU');
    $product->stock_status = $request->input('stock_status');
    $product->featured = $request->input('featured', false);
    $product->quantity = $request->input('quantity');
    $product->image = $imagePath;
    $product->category_id = $request->input('category_id');
    $product->brand_id = $request->input('brand_id');
    $product->discount_id = $request->input('discount_id');
    $product->save();

    // Redirect back with success message or wherever you want
    return redirect()->route('adminproduct.index')->with('success', 'Product created successfully.');
}

public function productedit($id)
{
    $product = Product::findOrFail($id);
    $categories = Category::all(); // Fetch categories
    $brands = Brand::all(); // Fetch brands
    $discounts = Discount::all(); // Fetch discounts
    // Pass the fetched data to the view
    return view('admin.products.edit', compact('product', 'categories', 'brands', 'discounts'));
}




public function update(Request $request, $id)
{
    $product = Product::findOrFail($id);

    $request->validate([
        'name' => 'required',
        'slug' => 'required|unique:products,slug,'.$id,
        'description' => 'required',
        'regular_price' => 'required|numeric|min:0',
        'sale_price' => 'nullable|numeric|min:0',
        'SKU' => 'nullable|unique:products,SKU,'.$id,
        'stock_status' => 'required|in:instock,outofstock',
        'featured' => 'boolean',
        'quantity' => 'required|integer|min:0',
        'image' => 'nullable|image|mimes:jpeg,png,jpg,gif|max:2048', // Adjust the max file size as needed
        'category_id' => 'required|exists:categories,id',
        'brand_id' => 'required|exists:brands,id',
        'discount_id' => 'nullable|exists:discounts,id',
    ]);

    if ($request->hasFile('image')) {
        $imagePath = $request->file('image')->store('products', 'public');
        $product->image = $imagePath;
    }

    $product->update($request->except('image'));

    return redirect()->route('adminproduct.index')->with('success', 'Product updated successfully.');
}

    public function productdelete($id)
{
    $product = Product::findOrFail($id);
    $product->delete();

    return redirect()->route('adminproduct.index')->with('success', 'Product deleted successfully.');
}



}
