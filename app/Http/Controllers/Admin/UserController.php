<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Log;
use Illuminate\Support\Facades\Hash; 

class UserController extends Controller
{

    public function userscreate()
{
    return view('admin.users.userscreate');
}


public function usersstore(Request $request)
{
    $request->validate([
        'name' => 'required|string|max:255',
        'email' => 'required|string|email|max:255|unique:users',
        'password' => 'required|string|min:8', 
    ]);

    $user = new User([
        'name' => $request->input('name'),
        'email' => $request->input('email'),
        'password' => Hash::make($request->input('password')), 
    ]);
    $user->save();

    return redirect()->route('adminusers.index')->with('success', 'User created successfully');
}

    public function userindex(Request $request)
    {
        $users = User::all();
        return view('admin.users.userindex', ['users' => $users]); 
    }

    public function useredit($id)
{
    $user = User::findOrFail($id);
    
    return view('admin.users.useredit', compact('user'));
}

public function userupdate(Request $request, $id)
{
    $user = User::findOrFail($id);

    $request->validate([
        'name' => 'required|string|max:255',
        'email' => 'required|email|max:255',
        'old_password' => 'required',
        'password' => 'nullable|string|min:8|confirmed',
    ]);

    if (!Hash::check($request->old_password, $user->password)) {
        return back()->withErrors(['old_password' => 'The current password is incorrect.']);
    }

    if ($request->filled('password')) {
        $user->password = Hash::make($request->password);
    }

    $user->name = $request->name;
    $user->email = $request->email;
    $user->save();

    return redirect()->route('adminusers.index')->with('success', 'Customer updated successfully.');

}

public function userdelete($id)
{
    $user = User::findOrFail($id);

    $user->delete();

    return redirect()->route('admin.users.userindex')->with('success', 'User deleted successfully');
}

   
}
