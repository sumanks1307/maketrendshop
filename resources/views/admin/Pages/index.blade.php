@extends('admin.layouts.admin')
@section('content')
<div class="content-wrapper">
    <div class="tab-content tab-content-basic">
        <div class="tab-pane fade show active" id="overview" role="tabpanel" aria-labelledby="overview">

            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('admin.index') }}">Dashboard</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Page Details</li>
                </ol>
            </nav>
          
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="d-flex justify-content-between align-items-center mb-2">
                            <h2>Static Pages</h2>
                            <a href="{{ route('admin.pages.create') }}" class="btn btn-success mb-2">Create Page</a>
                        </div>
                      
                            <table class="table-bordered">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Name</th>
                                        <th>Slug</th>
                                        <th>Description</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($pages as $page)
                                    <tr>
                                        <td>{{ $page->id }}</td>
                                        <td>{{ $page->title }}</td>
                                        <td>{{ $page->slug }}</td>
                                        <td>{{ $page->content }}</td>
                                        <td>
                                            <a href="{{ route('admin.static_pages.edit', $page) }}" class="btn btn-primary btn-sm">Edit</a>
                                            <form action="{{ route('admin.static_pages.destroy', $page) }}" method="POST" style="display:inline;">
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                                            </form>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                     
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
