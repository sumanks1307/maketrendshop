@extends('admin.layouts.admin')
@section('content')
<div class="content-wrapper">
    <div class="row">
        <div class="tab-content tab-content-basic">
            <div class="tab-pane fade show active" id="overview" role="tabpanel" aria-labelledby="overview">
                <!-- Breadcrumbs -->
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('admin.index') }}">Dashboard</a></li>
                    </ol>
                </nav>
                <!-- End Breadcrumbs -->

                <h2>Admin Dashboard - Working on it</h2>
            </div>
        </div>
    </div>
</div>
@endsection
