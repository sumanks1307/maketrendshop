<?php 
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Admin; // Import the Admin model

class Media extends Model
{
    use HasFactory;
    
    protected $fillable = ['file_name', 'file_path', 'admin_id'];

    public function admin()
    {
        return $this->belongsTo(Admin::class);
    }
}
