<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Brand;
use Illuminate\Support\Facades\Storage;


class BrandsController extends Controller
{
    //

    public function brandindex(Request $request)
    {
        $brands =Brand ::all();
        return view('admin.brands.index', ['brands' => $brands]); 
    }

    public function brandcreate()
    {
        return view('admin.brands.brandcreate');
    }
    public function brandstore(Request $request)
{
    $request->validate([
        'name' => 'required|unique:brands',
        'slug' => 'required|unique:brands',
        'description' => 'required',
        'image' => 'nullable|image|mimes:jpeg,png,jpg,gif|max:2048',
        'status' => 'nullable',
    ]);

    $imagePath = null;

    if ($request->hasFile('image')) {
        $image = $request->file('image');
        $imageName = time().'.'.$image->getClientOriginalExtension();

        // Store the image in the specified directory within the 'public' disk
        $imagePath = $image->storeAs('brand_images', $imageName, 'public');
    }

    $brand = Brand::create([
        'name' => $request->input('name'),
        'slug' => $request->input('slug'),
        'description' => $request->input('description'),
        'image_path' => $imagePath, 
        'status' => $request->input('status', '1'), // Set default value if not provided
    ]);

    return redirect()->route('adminbrands.index')->with('success', 'Brand created successfully.');
}

    

public function brandedit($id)
{
    $brand = Brand::findOrFail($id); // Fetch the brand based on ID
    return view('admin.brands.brandedit', compact('brand'));
}

public function brandupdate(Request $request, $id)
{
    $request->validate([
        'name' => 'required|unique:brands,name,' . $id,
        'slug' => 'required|unique:brands,slug,' . $id,
        'description' => 'required',
        'image' => 'nullable|image|mimes:jpeg,png,jpg,gif|max:2048',
        'status' => 'nullable',
    ]);

    $brand = Brand::findOrFail($id);
    $imagePath = $brand->image_path;

    if ($request->hasFile('image')) {
        // Delete the old image if exists
        if ($imagePath) {
            Storage::disk('public')->delete($imagePath);
        }

        $image = $request->file('image');
        $imageName = time().'.'.$image->getClientOriginalExtension();
        $imagePath = $image->storeAs('brand_images', $imageName, 'public');
    }

    $brand->update([
        'name' => $request->input('name'),
        'slug' => $request->input('slug'),
        'description' => $request->input('description'),
        'image_path' => $imagePath,
        'status' => $request->input('status', '1'),
    ]);

    return redirect()->route('adminbrands.index')->with('success', 'Brand updated successfully.');
}


public function branddelete($id)
{
    $brand = Brand::findOrFail($id);

    $brand->delete();

    return redirect()->route('adminbrands.index')->with('success', 'Brand deleted successfully');
}


 
}
