<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    use HasFactory;

    protected $fillable = [
        'sale_title',
        'product_title',
        'promotion',
        'new_price',
        'old_price',
        'description'
    ];

    public function images()
    {
        return $this->hasMany(BannerImage::class);
    }
}


