@extends('admin.layouts.admin')

@section('content')
<div class="content-wrapper">
    <div class="tab-content tab-content-basic">
        <div class="tab-pane fade show active" id="overview" role="tabpanel" aria-labelledby="overview">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('admin.index') }}">Dashboard</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Css Details</li>
                </ol>
            </nav>

            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        @if (session('status'))
                        <div class="status-message">{{ session('status') }}</div>
                        @endif
                        <form action="{{ route('admin.save_css') }}" method="POST">
                            @csrf
                            <div>
                                <label for="custom_css">Enter Custom CSS:</label>
                                <textarea id="custom_css" name="custom_css" rows="10" cols="80">{{ $customCss }}</textarea>
                            </div>
                            <button type="submit">Save CSS</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection



@push('scripts')
    <script src="https://cdn.ckeditor.com/ckeditor5/37.0.0/classic/ckeditor.js"></script>
    <script>
        ClassicEditor
            .create(document.querySelector('#custom_css'))
            .catch(error => {
                console.error(error);
            });
    </script>
@endpush
