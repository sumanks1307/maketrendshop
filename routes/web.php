<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\AppController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\ShopController; 
use App\Http\Controllers\HomeController;  


Route::get('/', [AppController::class, 'index'])->name('app.index');
Route::get('/shop', [ShopController::class, 'index'])->name('shop');
Route::get('/shop/{slug}', [ShopController::class, 'ProductDetails'])->name('shop.product.detail');

Route::get('home/{slug}', [AppController::class, 'showpage'])->name('homepages');
Route::get('/banner', [AppController::class, 'getBanner']);



Auth::routes();


Route::middleware('auth')->group(function () {
    Route::get('/myaccount', [UserController::class,'index'])->name('user.index');
});

// Route::middleware(['auth','auth.admin'])->group(function () {
//     Route::get('/admin', [AdminController::class, 'index'])->name('admin.index');
// });

// Admin related Routes

// Route::get('admin/login',[AdminController::class,'adminlogin'])->name('admin.login');
// Route::post('admin/login',[AdminController::class,'adminstore'])->name('login.store');
// Route::group(['middleware'=>'admin'],function(){
//     Route::get('/admin/dashboard', [AdminController::class, 'index'])->name('admin.index');
//     Route::get('logout',[AdminController::class,'Adminlogout'])->name('logout');

// });


