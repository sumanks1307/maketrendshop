<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Discount;

class DiscountController extends Controller
{
    public function discountindex()
    {
        $discounts = Discount::all();
        return view('admin.discount.index', compact('discounts'));
    }

    public function discountcreate()
    {
        return view('admin.discount.create');
    }

    public function discountedit($id)
    {
        $discount = Discount::findOrFail($id);
        return view('admin.discount.edit', compact('discount'));
    }

    public function discountdelete($id)
    {
        $discount = Discount::findOrFail($id);
        $discount->delete();

        return redirect()->route('admindiscount.index')->with('success', 'Discount deleted successfully.');
    }

    public function discountstore(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:discounts,name',
            'slug' => 'required|unique:discounts,slug',
            'description' => 'required',
            'rate' => 'required|numeric|min:0',
            'status' => 'nullable|boolean',
        ]);

        Discount::create([
            'name' => $request->input('name'),
            'slug' => $request->input('slug'),
            'description' => $request->input('description'),
            'rate' => $request->input('rate'),
            'status' => $request->input('status', 1), // Default to active if not provided
        ]);

        return redirect()->route('admindiscount.index')->with('success', 'Discount created successfully.');
    }

    public function discountupdate(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|unique:discounts,name,' . $id,
            'slug' => 'required|unique:discounts,slug,' . $id,
            'description' => 'required',
            'rate' => 'required|numeric|min:0',
            'status' => 'nullable|boolean',
        ]);

        $discount = Discount::findOrFail($id);
        $discount->update([
            'name' => $request->input('name'),
            'slug' => $request->input('slug'),
            'description' => $request->input('description'),
            'rate' => $request->input('rate'),
            'status' => $request->input('status', 1), // Default to active if not provided
        ]);

        return redirect()->route('admindiscount.index')->with('success', 'Discount updated successfully.');
    }
}
