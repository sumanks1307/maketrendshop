@extends('admin.layouts.admin')
@section('content')
<div class="content-wrapper">
    <div class="tab-content tab-content-basic">
        <div class="tab-pane fade show active" id="overview" role="tabpanel" aria-labelledby="overview">

            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('admin.index') }}">Dashboard</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Discount Details</li>
                </ol>
            </nav>
          
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="d-flex justify-content-between align-items-center mb-2">
                            <h2>Discounts</h2>
                            <a href="{{ route('admin.discount.create') }}" class="btn btn-success mb-2">Create Discount</a>
                        </div>
                        <table class="table table-bordered table-hover dt-responsive">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Slug</th>
                                    <th>Description</th>
                                    <th>Rate</th>
                                    <th>Status</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($discounts as $discount)
                                <tr>
                                    <td>{{ $discount->id }}</td>
                                    <td>{{ $discount->name }}</td>
                                    <td>{{ $discount->slug }}</td>
                                    <td>{{ $discount->description }}</td>
                                    <td>{{ $discount->rate }}%</td>
                                    <td style="color: {{ $discount->status == 1 ? 'green' : 'red' }}">
                                        {{ $discount->status == 1 ? 'Active' : 'Inactive' }}
                                    </td>
                                    <td>
                                        <a href="{{ route('admin.discount.edit', ['id' => $discount->id]) }}" class="btn btn-primary btn-sm">Edit</a>
                                        <form action="{{ route('admin.discount.delete', ['id' => $discount->id]) }}" method="POST" style="display: inline;">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
