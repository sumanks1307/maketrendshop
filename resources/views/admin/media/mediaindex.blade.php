@extends('admin.layouts.admin')
@section('content')
<div class="content-wrapper">
    <!-- Breadcrumb Navigation -->
    <div class="container-fluid">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('admin.index') }}">Dashboard</a></li>
                <li class="breadcrumb-item active" aria-current="page">Media</li>
            </ol>
        </nav>
    </div>

    <div class="tab-content tab-content-basic">
        <div class="tab-pane fade show active" id="overview" role="tabpanel" aria-labelledby="overview">
            <div class="media-header d-flex justify-content-between align-items-center">
                <h2>Media</h2>
                <a href="{{ route('admin.media.mediacreate') }}" class="btn btn-success">Upload Media</a>
            </div>
            <hr>
            <div class="container">
                <div class="row">
                    @if ($media->count() > 0)
                        @foreach ($media as $image)
                            <div class="col-xs-12 col-sm-6 col-md-3 mb-4">
                                <div class="image-container p-2 bordered">
                                    <div class="dropdown">
                                        <a class="dropdown-toggle mdi mdi-dots-vertical" href="#" role="button" id="dropdownMenuLink{{ $image->id }}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></a>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuLink{{ $image->id }}">
                                            <a class="dropdown-item" href="#" onclick="copyImagePath('{{ asset('storage/' . $image->file_path) }}')"><i class="mdi mdi-content-copy"></i> Copy Path</a>
                                            <form action="{{ route('admin.media.delete', ['id' => $image->id]) }}" method="POST">
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" class="dropdown-item"><i class="mdi mdi-delete"></i> Delete</button>
                                            </form>
                                        </div>
                                    </div>
                                    <!-- Add checkbox to select image -->
                                    <input type="checkbox" class="image-checkbox" value="{{ $image->id }}">
                                    <img class="img-fluid" src="{{ asset('storage/' . $image->file_path) }}" alt="{{ $image->file_name }}">
                                </div>
                            </div>
                        @endforeach
                    @else
                        <div class="col-xs-12">
                            <p>No images uploaded yet.</p>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>   
</div>

<!-- Script to copy image path to clipboard -->
<script>
    function copyImagePath(imagePath) {
        // Create a temporary input element
        var tempInput = document.createElement('input');
        tempInput.setAttribute('type', 'text');
        tempInput.setAttribute('value', imagePath);
        document.body.appendChild(tempInput);
        
        // Select the input element and copy its content
        tempInput.select();
        document.execCommand('copy');

        // Remove the temporary input element
        document.body.removeChild(tempInput);

        // Alert the user that the path has been copied
        alert('Image path copied');
    }
</script>
@endsection
