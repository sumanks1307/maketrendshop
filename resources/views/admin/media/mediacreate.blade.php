@extends('admin.layouts.admin')
@section('content')
<div class="content-wrapper">
    <!-- Breadcrumb Navigation -->
    <div class="container-fluid">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('admin.index') }}">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="{{ route('adminmedia.index') }}">Media</a></li>
                <li class="breadcrumb-item active" aria-current="page">Media Upload</li>
            </ol>
        </nav>
    </div>

    <div class="tab-content tab-content-basic">
        <div class="tab-pane fade show active" id="overview" role="tabpanel" aria-labelledby="overview">
            <h2>Media Upload</h2>
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <form action="{{ route('admin.media.uploadstore') }}" method="post" id="mediaUploadForm" enctype="multipart/form-data">
                            @csrf <!-- CSRF token -->
                            <input type="file" name="media" id="mediaUpload" accept="image/*,video/*">
                            <div id="previewContainer" style="display: none;">
                                <div id="preview"></div>
                            </div>
                            <br><br>
                            <button class="media-submit-btn" type="submit">Upload</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    document.addEventListener("DOMContentLoaded", function () {
        var mediaUploadForm = document.getElementById("mediaUploadForm");
        var mediaUpload = document.getElementById("mediaUpload");
        var previewContainer = document.getElementById("previewContainer");
        var preview = document.getElementById("preview");

        mediaUpload.addEventListener("change", function (event) {
            var file = event.target.files[0];
            if (file) {
                if (!validateFile(file)) {
                    toastr.error("Please upload a valid image or video file.");
                    mediaUpload.value = "";
                    return false; // Prevent form submission if validation fails
                }

                if (file.type.startsWith("image/")) {
                    // Show preview for images
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        preview.innerHTML = '<img src="' + e.target.result + '" alt="Preview">';
                    };
                    reader.readAsDataURL(file);
                    previewContainer.style.display = "block";
                } else {
                    // Hide preview for non-image files
                    previewContainer.style.display = "none";
                }
            } else {
                // Hide preview if no file selected
                previewContainer.style.display = "none";
            }
        });

        mediaUploadForm.addEventListener("submit", function (event) {
            // Validate file here again if needed
            var file = mediaUpload.files[0];
            if (!file || !validateFile(file)) {
                toastr.error("Please upload a valid image or video file.");
                event.preventDefault(); // Prevent form submission if validation fails
            }
        });

        function validateFile(file) {
            var allowedExtensions = /(\.jpg|\.jpeg|\.png|\.gif|\.mp4|\.avi|\.mov)$/i;
            return allowedExtensions.test(file.name);
        }
    });
</script>

@endsection
