<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Tax;


class TaxController extends Controller
{
    public function Taxindex()
    {
        $taxes = Tax::all();
        return view('admin.tax.index', compact('taxes'));
    }


    public function Taxcreate()
    {
        return view('admin.tax.taxcreate');
    }


    public function Taxtedit($id)
    {
        $tax = Tax::findOrFail($id);
        return view('admin.tax.taxedit', compact('tax'));
    }


    public function taxdelete($id)
    {
        $tax = Tax::findOrFail($id);
        $tax->delete();

        return redirect()->route('admintax.index')->with('success', 'Tax deleted successfully.');
    }

    public function TaxStore(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:taxes,name',
            'slug' => 'required|unique:taxes,slug',
            'description' => 'required',
            'rate' => 'required|numeric|min:0',
            'status' => 'nullable|boolean',
        ]);

        Tax::create([
            'name' => $request->input('name'),
            'slug' => $request->input('slug'),
            'description' => $request->input('description'),
            'rate' => $request->input('rate'),
            'status' => $request->input('status', 1), // Default to active if not provided
        ]);

        return redirect()->route('admintax.index')->with('success', 'Tax created successfully.');
    }


    public function taxupdate(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|unique:taxes,name,' . $id,
            'slug' => 'required|unique:taxes,slug,' . $id,
            'description' => 'required',
            'rate' => 'required|numeric|min:0',
            'status' => 'nullable|boolean',
        ]);

        $tax = Tax::findOrFail($id);
        $tax->update([
            'name' => $request->input('name'),
            'slug' => $request->input('slug'),
            'description' => $request->input('description'),
            'rate' => $request->input('rate'),
            'status' => $request->input('status', 1), // Default to active if not provided
        ]);

        return redirect()->route('admintax.index')->with('success', 'Tax updated successfully.');
    }
}
