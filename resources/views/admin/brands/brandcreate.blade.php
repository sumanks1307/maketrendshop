@extends('admin.layouts.admin') 
@section('content')
<div class="content-wrapper">
    <div class="tab-content tab-content-basic">
        <div class="tab-pane fade show active" id="overview" role="tabpanel" aria-labelledby="overview">
            <!-- Breadcrumbs -->
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('admin.index') }}">Dashboard</a></li>
                    <li class="breadcrumb-item active" aria-current="page"> Brands</li>
                </ol>
            </nav>
            <!-- End Breadcrumbs -->
            <h2>Create Brands</h2>
            <div class="container">
           
                    <div class="col-md-12">
                        <form method="POST" action="{{ route('admin.brands.brandstore') }}" enctype="multipart/form-data" id="createBrandForm">
                            @csrf
                            <div class="row">
                                
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="name">Name</label>
                                        <input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}"  minlength="2" maxlength="255">
                                        <div class="invalid-feedback">
                                            Please provide a valid name.
                                        </div>
                                        @error('name')
                                        <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                            
                                <div class="col-md-6">
                                   <div class="form-group">
                                   <label for="slug">Slug</label>
                                   <input type="text" class="form-control" id="slug" name="slug" value="{{ old('slug') }}"  readonly>
                                   @error('slug')
                                   <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="description">Description</label>
                                        <textarea class="form-control" id="description" name="description" >{{ old('description') }}</textarea>
                                        <div class="invalid-feedback">
                                            Please provide a valid description.
                                        </div>
                                        @error('description')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>

                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="status">Status</label>
                                        <select class="form-control" id="status" name="status">
                                            <option value="1">Active</option>
                                            <option value="0">Inactive</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <!-- Image Upload -->
                                    <div class="form-group">
                                        <label for="image">Image</label>
                                        <input type="file" class="form-control-file" id="image" name="image" onchange="validateImage(event)" >
                                        <div class="invalid-feedback">
                                            Please select an image file.
                                        </div>
                                        <img id="image_preview" src="#" alt="Image Preview" style="display: none; max-width: 100%; margin-top: 10px;">
                                        @error('image')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>

                            </div>
                            <button type="submit" class="btn btn-primary">Create Brand</button>
                        </form>
                    </div>
            </div>
        </div>
    </div>
</div>


<script>
    function validateImage(event) {
        var file = event.target.files[0];
        var fileType = file.type.split('/')[0];
        if (fileType !== 'image') {
            event.target.setCustomValidity('Please select an image file.');
        } else {
            event.target.setCustomValidity('');
            var reader = new FileReader();
            reader.onload = function(){
                var output = document.getElementById('image_preview');
                output.src = reader.result;
                output.style.display = 'block'; 
            }
            reader.readAsDataURL(file);
        }
    }

    // Auto-generate slug based on the name
    document.getElementById('name').addEventListener('input', function() {
        var name = this.value.trim().toLowerCase().replace(/[^a-z0-9]+/g, '-');
        document.getElementById('slug').value = name;
    });

    // Form validation using plain JavaScript
    document.getElementById('createBrandForm').addEventListener('submit', function(event) {
        var name = document.getElementById('name').value.trim();
        var description = document.getElementById('description').value.trim();
        var image = document.getElementById('image').value.trim();

        if (!name || !description || !image) {
            event.preventDefault(); // Prevent form submission
            // Display error messages
            if (!name) {
                document.getElementById('name').classList.add('is-invalid');
            }
            if (!description) {
                document.getElementById('description').classList.add('is-invalid');
            }
            if (!image) {
                document.getElementById('image').classList.add('is-invalid');
            }
        }
    });
</script>

@endsection
