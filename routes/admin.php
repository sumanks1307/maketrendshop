<?php 

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Admin\BrandsController;
use App\Http\Controllers\Admin\MediaController;
use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\TaxController;
use App\Http\Controllers\Admin\DiscountController;
use App\Http\Controllers\Admin\CouponController;
use App\Http\Controllers\Admin\ProductController;
use App\Http\Controllers\Admin\PageController;
use App\Http\Controllers\Admin\SystemController;
use App\Http\Controllers\Admin\BannerController;


// login routes
Route::get('admin/login',[AdminController::class,'adminlogin'])->name('admin.login');
Route::post('admin/login',[AdminController::class,'adminstore'])->name('login.store');

Route::group(['middleware'=>'admin'],function(){

Route::get('/admin/dashboard', [AdminController::class, 'index'])->name('admin.index');
Route::get('logout',[AdminController::class,'Adminlogout'])->name('logout');

// customer paenel
Route::get('/admin/users/create', [UserController::class,'userscreate'])->name('admin.users.create');
Route::post('/admin/users/store', [UserController::class,'usersstore'])->name('admin.users.store');
    
Route::get('/admin/users', [UserController::class, 'userindex'])->name('adminusers.index');
Route::get('/admin/users/{id}/edit', [UserController::class,'useredit'])->name('admin.users.edit');
Route::put('/admin/users/{id}', [UserController::class,'userupdate'])->name('admin.users.update');
Route::delete('/admin/users/{id}', [UserController::class,'userdelete'])->name('admin.users.delete');

// brands sections
Route::get('/admin/brand', [BrandsController::class, 'brandindex'])->name('adminbrands.index');
Route::get('/admin/brand/create', [BrandsController::class,'brandcreate'])->name('admin.brands.brandcreate');
Route::post('/admin/brand/store', [BrandsController::class,'brandstore'])->name('admin.brands.brandstore');
Route::get('/admin/brand/{id}/edit', [BrandsController::class,'brandedit'])->name('admin.brand.edit');
Route::put('/admin/brand/{id}', [BrandsController::class,'brandupdate'])->name('admin.brand.update');
Route::delete('/admin/brand/{id}', [BrandsController::class,'branddelete'])->name('admin.brand.delete');


// Media routes
Route::get('/admin/media', [MediaController::class, 'mediaindex'])->name('adminmedia.index');
Route::get('/admin/media/create', [MediaController::class,'mediacreate'])->name('admin.media.mediacreate');
Route::post('/admin/media/upload', [MediaController::class,'mediaupload'])->name('admin.media.uploadstore');
Route::delete('/admin/media/{id}', [MediaController::class,'mediadelete'])->name('admin.media.delete');



// category sections
Route::get('/admin/category', [CategoryController::class, 'Categoryindex'])->name('admincategory.index');
Route::get('/admin/category/create', [CategoryController::class,'categorycreate'])->name('admin.category.categorycreate');
Route::post('/admin/category/store', [CategoryController::class,'categoryStore'])->name('admin.category.categorystore');
Route::delete('/admin/category/{id}', [CategoryController::class,'categorydelete'])->name('admin.category.delete');
Route::get('/admin/category/{id}/edit', [CategoryController::class,'categoryedit'])->name('admin.category.edit');
Route::put('/admin/category/{id}', [CategoryController::class,'categoryupdate'])->name('admin.category.update');


// Tax sections

Route::get('/admin/tax', [TaxController::class, 'Taxindex'])->name('admintax.index');
Route::get('/admin/tax/create', [TaxController::class,'Taxcreate'])->name('admin.tax.taxcreate');
Route::post('/admin/tax/store', [TaxController::class,'TaxStore'])->name('admin.tax.store');
Route::delete('/admin/tax/{id}', [TaxController::class,'taxdelete'])->name('admin.tax.delete');
Route::get('/admin/tax/{id}/edit', [TaxController::class,'Taxtedit'])->name('admin.tax.edit');
Route::put('/admin/tax/{id}', [TaxController::class,'taxupdate'])->name('admin.tax.update');


// discount 
Route::get('/admin/discount', [DiscountController::class, 'discountindex'])->name('admindiscount.index');
Route::get('/admin/discount/create', [DiscountController::class, 'discountcreate'])->name('admin.discount.create');
Route::post('/admin/discount/store', [DiscountController::class, 'discountstore'])->name('admin.discount.store');
Route::delete('/admin/discount/{id}', [DiscountController::class, 'discountdelete'])->name('admin.discount.delete');
Route::get('/admin/discount/{id}/edit', [DiscountController::class, 'discountedit'])->name('admin.discount.edit');
Route::put('/admin/discount/{id}', [DiscountController::class, 'discountupdate'])->name('admin.discount.update');


//coupon
Route::get('/admin/coupons', [CouponController::class, 'index'])->name('admincoupons.index');
Route::get('/admin/coupons/create', [CouponController::class, 'create'])->name('admin.coupons.create');
Route::post('/admin/coupons/store', [CouponController::class, 'store'])->name('admin.coupons.store');
Route::get('/admin/coupons/{id}/edit', [CouponController::class, 'edit'])->name('admin.coupons.edit');
Route::put('/admin/coupons/{id}', [CouponController::class, 'update'])->name('admin.coupons.update');
Route::delete('/admin/coupons/{id}', [CouponController::class, 'destroy'])->name('admin.coupons.destroy');



//products

Route::get('/admin/product', [ProductController::class, 'productindex'])->name('adminproduct.index');
Route::get('product/create', [ProductController::class, 'create'])->name('admin.product.create');
Route::post('product/store', [ProductController::class, 'store'])->name('admin.products.store');
Route::get('product/{id}/edit', [ProductController::class, 'productedit'])->name('admin.product.edit');
Route::put('product/{id}', [ProductController::class, 'update'])->name('admin.product.update');
Route::delete('product/{id}', [ProductController::class, 'productdelete'])->name('admin.product.delete');


// static pages
Route::get('pages',[PageController::class, 'PagesIndex'])->name('admin.pages.index');
Route::get('pages/create', [PageController::class, 'pagecreate'])->name('admin.pages.create');
Route::post('pages', [PageController::class, 'store'])->name('admin.pages.store');
Route::get('pages/{staticPage}/edit', [PageController::class, 'edit'])->name('admin.static_pages.edit');
Route::put('pages/{staticPage}', [PageController::class, 'update'])->name('admin.static_pages.update');
Route::delete('static_pages/{staticPage}', [PageController::class, 'destroy'])->name('admin.static_pages.destroy');


// custom  css
Route::get('/admin/css', [SystemController::class, 'index'])->name('admin.css');
Route::post('/admin/save-css', [SystemController::class, 'saveCss'])->name('admin.save_css');

// Banners

// routes/web.php


// Route  all banners
Route::get('/admin/banner', [BannerController::class, 'index'])->name('admin.banner.index');
Route::get('/admin/banner/create', [BannerController::class, 'create'])->name('admin.banner.create');
Route::post('/admin/banner/store', [BannerController::class, 'store'])->name('admin.bannerstore');
Route::delete('/admin/banner/{id}', [BannerController::class, 'delete'])->name('admin.banner.delete');
Route::get('/admin/banner/{id}/edit', [BannerController::class, 'edit'])->name('admin.banner.edit');
Route::put('/admin/banner/{id}', [BannerController::class, 'update'])->name('admin.banner.update');





});