@extends('admin.layouts.admin')
@section('content')

<div class="content-wrapper">
    <div class="tab-content tab-content-basic">
        <div class="tab-pane fade show active" id="overview" role="tabpanel" aria-labelledby="overview">
            <!-- Breadcrumbs -->
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('admin.index') }}">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="{{ route('admincategory.index') }}">Categories</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Category List</li>
                </ol>
            </nav>
            <!-- End Breadcrumbs -->
            <div class="d-flex justify-content-between align-items-center mb-2">
                <h2>Category</h2>
                <a href="{{ route('admin.category.categorycreate') }}" class="btn btn-success">Create Category</a>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <table class="table table-bordered table-hover dt-responsive">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Slug</th>
                                    <th>Description</th>
                                    <th>Image</th>
                                    <th>Status</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($categories as $category)
                                <tr>
                                    <td>{{ $category->id }}</td>
                                    <td>{{ $category->name }}</td>
                                    <td>{{ $category->slug }}</td>
                                    <td>{{ $category->description }}</td>
                                    <td>
                                        @if($category->image)
                                            <img src="{{ url('storage/' . $category->image) }}" alt="{{ $category->name }}" style="width: 100px; height: auto;">
                                        @else
                                            <p>No image</p>
                                        @endif
                                    </td>
                                    <td style="color: {{ $category->status == 1 ? 'green' : 'red' }}">
                                        {{ $category->status == 1 ? 'Active' : 'Inactive' }}
                                    </td>
                                    <td>
                                        <a href="{{ route('admin.category.edit', ['id' => $category->id]) }}" class="btn btn-primary btn-sm">Edit</a>
                                        <form action="{{ route('admin.category.delete', ['id' => $category->id]) }}" method="POST" style="display: inline;">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
     
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
