<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\StaticPage;
use App\Models\Banner;
class AppController extends Controller
{
    

    public function index()
    {
        return view("front.index");
    }

    
    public function showpage($slug)
    {
        // return view('front.homepages.about');
        $page = StaticPage::where('slug', $slug)->firstOrFail();
        return view('front.homepages.staticpage', compact('page'));
    }

    public function getBanner(Request $request)
    {
        $banners = Banner::all();
        return view('front.sections.banner', compact('banners'));
    }
}
