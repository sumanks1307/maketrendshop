<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Faker\Generator as Faker;
use App\Models\Product;

class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'name' => $this->faker->word,
            'slug' => $this->faker->slug,
            'image' => $this->faker->imageUrl(),
            'short_description' => $this->faker->sentence,
            'description' => $this->faker->paragraph,
            'regular_price' => $this->faker->randomFloat(2, 10, 100),
            'sale_price' => $this->faker->optional(0.5, 0)->randomFloat(2, 5, 50),
            'SKU' => $this->faker->unique()->uuid,
            'stock_status' => $this->faker->randomElement(['instock']),
            'featured' => $this->faker->boolean,
            'quantity' => $this->faker->numberBetween(1, 100),
            'images' => json_encode($this->faker->randomElements(['image1.jpg', 'image2.jpg', 'image3.jpg'], 3)),
            'category_id' => function () {
                return \App\Models\Category::factory()->create()->id;
            },
            'brand_id' => function () {
                return \App\Models\Brand::factory()->create()->id;
            },
        ];
    }
}
