<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;
use Illuminate\Support\Facades\Storage;


class CategoryController extends Controller
{
    
    public function Categoryindex(Request $request)
    {
        $categories = Category::paginate(10);
      // $categories =Category ::all();
       return view('admin.category.index', ['categories' => $categories]); 
    }


    public function categorycreate()
    {

    $topCategories = Category::whereNull('parent_id')->get();

    // Pass topCategories to the view
    return view('admin.category.categorycreate', compact('topCategories'));
    }

    public function categoryStore(Request $request)
    {
        // Validate the incoming request data
        $request->validate([
            'name' => 'required|unique:categories',
            'slug' => 'required|unique:categories',
            'description' => 'required',
            'image' => 'nullable|image|mimes:jpeg,png,jpg,gif|max:2048',
            'status' => 'nullable|boolean',
            'parent_id' => 'nullable|exists:categories,id',
        ]);

        // Handle file upload
        $imagePath = null;
        if ($request->hasFile('image')) {
            $imagePath = $request->file('image')->store('category_images', 'public');
        }

        // Create the category
        $category = new Category();
        $category->name = $request->input('name');
        $category->slug = $request->input('slug');
        $category->description = $request->input('description');
        $category->image = $imagePath;
        $category->status = $request->input('status', 1); // Default to 1 if not provided
        $category->parent_id = $request->input('parent_id');
        $category->save();

        // Redirect back with success message
        return redirect()->route('admincategory.index')->with('success', 'Category created successfully.');
    }


    public function categoryedit($id)
    {
        $category = Category::findOrFail($id); // Fetch the category based on ID
        $categories = Category::whereNull('parent_id')->get(); // Fetch all top-level categories
    
        return view('admin.category.categoryedit', compact('category', 'categories'));
    }

    public function categoryUpdate(Request $request, $id)
{
    $request->validate([
        'name' => 'required|unique:categories,name,' . $id,
        'slug' => 'required|unique:categories,slug,' . $id,
        'description' => 'required',
        'image' => 'nullable|image|mimes:jpeg,png,jpg,gif|max:2048',
        'status' => 'nullable',
    ]);

    $category = Category::findOrFail($id);
    $imagePath = $category->image;

    if ($request->hasFile('image')) {
        // Delete the old image if it exists
        if ($imagePath) {
            Storage::disk('public')->delete($imagePath);
            // Flash message to indicate that the old image has been deleted
            session()->flash('success', 'Old image deleted successfully.');
        }

        $image = $request->file('image');
        $imageName = time().'.'.$image->getClientOriginalExtension();
        $imagePath = $image->storeAs('category_images', $imageName, 'public');
    }

    $category->update([
        'name' => $request->input('name'),
        'slug' => $request->input('slug'),
        'description' => $request->input('description'),
        'image' => $imagePath,
        'status' => $request->input('status', '1'),
    ]);

    return redirect()->route('admincategory.index')->with('success', 'Category updated successfully.');
}

    

    public function categorydelete($id)
{
    $category = Category::findOrFail($id);

    $category->delete();

    return redirect()->route('admincategory.index')->with('success', 'category deleted successfully');
}

}
