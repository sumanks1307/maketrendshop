@extends('admin.layouts.admin') 
@section('content')
<div class="content-wrapper">
    <div class="tab-content tab-content-basic">
        <div class="tab-pane fade show active" id="overview" role="tabpanel" aria-labelledby="overview">
            <!-- Breadcrumbs -->
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('admin.index') }}">Dashboard</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Banner</li>
                </ol>
            </nav>
            <!-- End Breadcrumbs -->
            <h2>Create Banner</h2>
            <div class="container">
                <div class="col-md-12">
                    <form action="{{ route('admin.bannerstore') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label for="main_image">Main Image</label>
                            <input type="file" name="main_image[]" id="main_image" class="form-control @error('main_image') is-invalid @enderror" multiple>
                            @error('main_image')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                            <div id="main_image_preview" class="image-preview"></div>
                        </div>
                        <div class="form-group">
                            <label for="thumbnail_image">Thumbnail Image</label>
                            <input type="file" name="thumbnail_image[]" id="thumbnail_image" class="form-control @error('thumbnail_image') is-invalid @enderror" multiple>
                            @error('thumbnail_image')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                            <div id="thumbnail_image_preview" class="image-preview"></div>
                        </div>
                        
                        <div class="form-group">
                            <label for="sale_title">Sale Title</label>
                            <input type="text" name="sale_title" id="sale_title" class="form-control @error('sale_title') is-invalid @enderror">
                            @error('sale_title')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="product_title">Product Title</label>
                            <input type="text" name="product_title" id="product_title" class="form-control @error('product_title') is-invalid @enderror">
                            @error('product_title')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="promotion">Promotion</label>
                            <input type="text" name="promotion" id="promotion" class="form-control @error('promotion') is-invalid @enderror">
                            @error('promotion')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="new_price">New Price</label>
                            <input type="number" step="0.01" name="new_price" id="new_price" class="form-control @error('new_price') is-invalid @enderror">
                            @error('new_price')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="old_price">Old Price</label>
                            <input type="number" step="0.01" name="old_price" id="old_price" class="form-control @error('old_price') is-invalid @enderror">
                            @error('old_price')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="description">Description</label>
                            <textarea name="description" id="description" class="form-control @error('description') is-invalid @enderror"></textarea>
                            @error('description')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <button type="submit" class="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script>
document.getElementById('main_image').addEventListener('change', function(event) {
    previewImages(event.target.files, 'main_image_preview');
});

document.getElementById('thumbnail_image').addEventListener('change', function(event) {
    previewImages(event.target.files, 'thumbnail_image_preview');
});

function previewImages(files, previewElementId) {
    let previewElement = document.getElementById(previewElementId);
    previewElement.innerHTML = '';
    for (let i = 0; i < files.length; i++) {
        let file = files[i];
        let reader = new FileReader();
        reader.onload = function(event) {
            let img = document.createElement('img');
            img.src = event.target.result;
            img.style.maxWidth = '100px';
            img.style.margin = '5px';
            previewElement.appendChild(img);
        }
        reader.readAsDataURL(file);
    }
}
</script>
@endsection
