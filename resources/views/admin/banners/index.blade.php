@extends('admin.layouts.admin')
@section('content')

<div class="content-wrapper">
    <div class="tab-content tab-content-basic">
        <div class="tab-pane fade show active" id="overview" role="tabpanel" aria-labelledby="overview">
            <!-- Breadcrumbs -->
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('admin.index') }}">Dashboard</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Banner</li>
                </ol>
            </nav>
            <!-- End Breadcrumbs -->
            <div class="container">
                <div class="d-flex justify-content-between align-items-center mb-2">
                    <h2>Banner</h2>
                    <a href="{{ route('admin.banner.create') }}" class="btn btn-success">Create Banner</a>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <table class="table mt-3">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Main Images</th>
                                    <th>Thumbnail Images</th>
                                    <th>Sale Title</th>
                                    <th>Product Title</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($banners as $banner)
                                    <tr>
                                        <td>{{ $banner->id }}</td>
                                        <td>
                                            @foreach ($banner->images->where('type', 'main') as $mainImage)
                                                <img src="{{ asset('storage/' . $mainImage->path) }}" width="50">
                                            @endforeach
                                        </td>
                                        <td>
                                            @foreach ($banner->images->where('type', 'thumbnail') as $thumbnailImage)
                                                <img src="{{ asset('storage/' . $thumbnailImage->path) }}" width="50">
                                            @endforeach
                                        </td>
                                        <td>{{ $banner->sale_title }}</td>
                                        <td>{{ $banner->product_title }}</td>
                                        <td>
                                            <a href="{{ route('admin.banner.edit', ['id' => $banner->id]) }}" class="btn btn-primary btn-sm">Edit</a>
                                            <form action="{{ route('admin.banner.delete', $banner->id) }}" method="POST" style="display:inline;">
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <!-- Pagination (if needed) -->
                        <div class="d-flex justify-content-center">
                            {{-- {{ $banners->links() }} --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
