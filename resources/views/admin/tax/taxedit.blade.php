@extends('admin.layouts.admin')
@section('content')
<div class="content-wrapper">
    <div class="container-fluid">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('admin.index') }}">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="{{ route('admintax.index') }}">Taxes</a></li>
                <li class="breadcrumb-item active" aria-current="page">Edit Tax</li>
            </ol>
        </nav>
    </div>
    <div class="tab-content tab-content-basic">
        <div class="tab-pane fade show active" id="overview" role="tabpanel" aria-labelledby="overview">
            <h2>Tax Edit Details</h2>
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <form method="POST" action="{{ route('admin.tax.update', $tax->id) }}">
                            @csrf
                            @method('PUT')

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="name">Name</label>
                                        <input type="text" class="form-control" id="name" name="name" value="{{ old('name', $tax->name) }}" required>
                                        @error('name')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="slug">Slug</label>
                                        <input type="text" class="form-control" id="slug" name="slug" value="{{ old('slug', $tax->slug) }}" required>
                                        @error('slug')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="description">Description</label>
                                        <textarea class="form-control" id="description" name="description" required>{{ old('description', $tax->description) }}</textarea>
                                        @error('description')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="rate">Rate (%)</label>
                                        <input type="number" step="0.01" class="form-control" id="rate" name="rate" value="{{ old('rate', $tax->rate) }}" required>
                                        @error('rate')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="status">Status</label>
                                        <select class="form-control" id="status" name="status">
                                            <option value="1" {{ $tax->status == 1 ? 'selected' : '' }}>Active</option>
                                            <option value="0" {{ $tax->status == 0 ? 'selected' : '' }}>Inactive</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                  
                            
                            <button type="submit" class="btn btn-primary">Update Tax</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    // Auto-generate slug based on the name
    document.getElementById('name').addEventListener('input', function() {
        var name = this.value.trim().toLowerCase().replace(/[^a-z0-9]+/g, '-');
        document.getElementById('slug').value = name;
    });
</script>

@endsection
