@extends('admin.layouts.admin')
@section('content')

<div class="content-wrapper">
    <div class="tab-content tab-content-basic">
        <div class="tab-pane fade show active" id="overview" role="tabpanel" aria-labelledby="overview">
            <!-- Breadcrumbs -->
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('admin.index') }}">Dashboard</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Brands</li>
                </ol>
            </nav>
            <!-- End Breadcrumbs -->
            <div class="container">
                <div class="d-flex justify-content-between align-items-center mb-2">
                    <h2>Brands</h2>
                    <a href="{{ route('admin.brands.brandcreate') }}" class="btn btn-success">Create Brands</a>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-bordered table-hover dt-responsive">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Slug</th>
                                    <th>Description</th>
                                    <th>Image</th>
                                    <th>Status</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($brands as $brand)
                                <tr>
                                    <td>{{ $brand->id }}</td>
                                    <td>{{ $brand->name }}</td>
                                    <td>{{ $brand->slug }}</td>
                                    <td>{{ $brand->description }}</td>
                                    <td>
                                        @if($brand->image_path)
                                            <img src="{{ url('storage/' . $brand->image_path) }}" alt="{{ $brand->name }}" style="width: 100px; height: auto;">
                                        @else
                                            <p>No image</p>
                                        @endif
                                    </td>
                                    <td style="color: {{ $brand->status == 1 ? 'green' : 'red' }}">
                                        {{ $brand->status == 1 ? 'Active' : 'Inactive' }}
                                    </td>
                                    <td>
                                        <a href="{{ route('admin.brand.edit', ['id' => $brand->id]) }}" class="btn btn-primary btn-sm">Edit</a>
                                        <form action="{{ route('admin.brand.delete', ['id' => $brand->id]) }}" method="POST" style="display: inline;">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
