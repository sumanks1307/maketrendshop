<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;

class ShopController extends Controller
{
     public function index()
     {
         $products = Product::orderBy('created_at', 'desc')->paginate(12);
         return view('front.shop', ['products' => $products]);
     }

     public function  ProductDetails($slug)
     {
          $product = Product::where('slug', $slug)->first();
          $rproducts = Product::where('slug',$slug);
          return view('front.details',['product'=>$product]);
          
     }
     
}
