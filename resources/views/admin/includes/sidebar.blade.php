<nav class="sidebar sidebar-offcanvas" id="sidebar">
    <ul class="nav">
        <li class="nav-item">
            <a class="nav-link" href="{{ route('admin.index') }}">
                <i class="mdi mdi-grid-large menu-icon"></i>
                <span class="menu-title">Dashboard</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ route('adminusers.index') }}">
                <i class="mdi mdi-account menu-icon"></i>
                <span class="menu-title">Customer</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ route('adminproduct.index') }}">
                <i class="mdi mdi-shopping menu-icon"></i>
                <span class="menu-title">Products</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ route('adminbrands.index') }}">
                <i class="mdi mdi-store menu-icon"></i>
                <span class="menu-title">Brands</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ route('admincategory.index') }}">
                <i class="mdi mdi-view-grid menu-icon"></i>
                <span class="menu-title">Category</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ route('admintax.index') }}">
                <i class="mdi mdi-currency-usd menu-icon"></i>
                <span class="menu-title">Tax</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ route('adminmedia.index') }}">
                <i class="mdi mdi-folder-multiple-image menu-icon"></i>
                <span class="menu-title">Media</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ route('admindiscount.index') }}">
                <i class="mdi mdi-percent menu-icon"></i>
                <span class="menu-title">Discount</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ route('admincoupons.index') }}">
                <i class="mdi mdi-ticket menu-icon"></i>
                <span class="menu-title">Coupon</span>
            </a>
        </li>

        <li class="nav-item">
            <a class="nav-link" href="{{ route('admin.css') }}">
                <i class="mdi mdi-grid-large menu-icon"></i>
                <span class="menu-title">CSS</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ route('admin.pages.index') }}">
                <i class="mdi mdi-book-open-page-variant menu-icon"></i>
                <span class="menu-title">Pages</span>
            </a>
        </li>

        <li class="nav-item">
            <a class="nav-link" href="{{route('admin.banner.index')}}">
                <i class="mdi mdi-book-open-page-variant menu-icon"></i>
                <span class="menu-title">Banners</span>
            </a>
        </li>
       
    </ul>
  </nav>
  