<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Media;
use App\Models\Admin;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;


class MediaController extends Controller
{
    public function mediaindex(Request $request)
    {
        $media = Media::all();
        // dd($media);
        return view('admin.media.mediaindex', ['media' => $media]); 
    }

    public function mediacreate()
    {
        return view('admin.media.mediacreate');
    }



    
    public function mediaupload(Request $request)
    {
        $request->validate([
            'media' => 'required|file|mimes:jpeg,png,gif,mp4,avi,mov|max:10240', // Maximum 10MB file size
        ]);
    
        $file = $request->file('media');
        $fileName = time() . '_' . $file->getClientOriginalName();
    
        // Create the directory if it doesn't exist
        $directory = 'public/media';
        if (!Storage::exists($directory)) {
            Storage::makeDirectory($directory);
        }
    
        // Store the media file in the specified directory
        $filePath = $file->storeAs('media', $fileName, 'public');
    
        // Save file information into the database
        $media = new Media();
        $media->file_name = $fileName;
        $media->file_path = $filePath;
        $media->admin_id = Auth::id(); 
        $media->save();
    
        return redirect()->route('adminmedia.index')->with('success', 'media uploaded successfully.');
    }

    public function mediadelete($id)
{
    $media = Media::findOrFail($id);

    $media->delete();

    return redirect()->route('adminmedia.index')->with('success', 'media deleted successfully');
}
    
    
    
}
