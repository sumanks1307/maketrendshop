@extends('front.layouts.base')
@section('content')

<section>
    <div class="container">
        <div class="row gx-4 gy-5">
            <div class="col-lg-12 col-12">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('app.index') }}">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">{{ ucwords(str_replace('-', ' ', $page->slug)) }}</li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="container">
            <h4 class="mb-4">
                <b>{{ $page->title }}</b>
            </h4>
            <div class="fs-24">
                {!! $page->content !!}
            </div>
        </div>
    </div>
</section>

@endsection
