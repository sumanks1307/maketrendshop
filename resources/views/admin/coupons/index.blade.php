@extends('admin.layouts.admin')

@section('content')
    <div class="content-wrapper">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('admin.index') }}">Dashboard</a></li>
                <li class="breadcrumb-item active" aria-current="page">Coupons Details</li>
            </ol>
        </nav>
        <div class="tab-content tab-content-basic">
            <div class="tab-pane fade show active" id="overview" role="tabpanel" aria-labelledby="overview">
               
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="d-flex justify-content-between align-items-center mb-2">
                                <h2>Coupons</h2>
                                <a href="{{ route('admin.coupons.create') }}" class="btn btn-success mb-2">Create Coupon</a>
    

                            </div>
                            <table class="table table-bordered table-hover dt-responsive">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Name</th>
                                        <th>Code</th>
                                        <th>Discount Type</th>
                                        <th>Discount Amount</th>
                                        <th>Expiry Date</th>
                                        <th>Status</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($coupons as $coupon)
                                        <tr>
                                            <td>{{ $coupon->id }}</td>
                                            <td>{{ $coupon->name }}</td>
                                            <td>{{ $coupon->code }}</td>
                                            <td>{{ ucfirst($coupon->discount_type) }}</td>
                                            <td>{{ $coupon->discount_amount }}</td>
                                            <td>{{ $coupon->expiry_date }}</td>
                                            <td>{{ $coupon->status ? 'Active' : 'Inactive' }}</td>
                                            <td>
                                                <a href="{{ route('admin.coupons.edit', $coupon->id) }}" class="btn btn-primary btn-sm">Edit</a>
                                                <form action="{{ route('admin.coupons.destroy', $coupon->id) }}" method="POST" style="display: inline;">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
