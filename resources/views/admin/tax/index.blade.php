@extends('admin.layouts.admin')
@section('content')

<div class="content-wrapper">
    <!-- Breadcrumb Navigation -->
    <div class="container-fluid">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('admin.index') }}">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="{{ route('admintax.index') }}">Taxes</a></li>
                <li class="breadcrumb-item active" aria-current="page">Tax List</li>
            </ol>
        </nav>
    </div>

    <div class="tab-content tab-content-basic">
        <div class="tab-pane fade show active" id="overview" role="tabpanel" aria-labelledby="overview">
            <div class="container">
                <div class="d-flex justify-content-between align-items-center mb-2">
                        <h2>Taxes</h2>
                        <a href="{{ route('admin.tax.taxcreate') }}" class="btn btn-success mb-2">Create Tax</a>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <table class="table table-bordered table-hover dt-responsive">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Slug</th>
                                    <th>Description</th>
                                    <th>Rate</th>
                                    <th>Status</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($taxes as $tax)
                                <tr>
                                    <td>{{ $tax->id }}</td>
                                    <td>{{ $tax->name }}</td>
                                    <td>{{ $tax->slug }}</td>
                                    <td>{{ $tax->description }}</td>
                                    <td>{{ $tax->rate }}%</td>
                                    <td style="color: {{ $tax->status == 1 ? 'green' : 'red' }}">
                                        {{ $tax->status == 1 ? 'Active' : 'Inactive' }}
                                    </td>
                                    <td>
                                        <a href="{{ route('admin.tax.edit', ['id' => $tax->id]) }}" class="btn btn-primary btn-sm">Edit</a>
                                        <form action="{{ route('admin.tax.delete', ['id' => $tax->id]) }}" method="POST" style="display: inline;">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
