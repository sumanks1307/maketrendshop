<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Session;

use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function index()
    {
        return view("admin.index");
    }

    public function adminlogin()
    {
        return view("admin.login");
    }

    public function adminstore(Request $request){
        $request->validate([
            'email'=>'required',
            'password'=>'required',
        ]);

        if (Auth::guard('admin')->attempt(['email' => $request->email, 'password' => $request->password])) {
            return redirect()->route('admin.index')->with('success', 'Admin Login successfully');
        
        }else{
            // Session::flash('error-message','Invalid Email or Password');
            return back()->with('error', 'Invalid Email or Password');
        }
    }

    public function Adminlogout(){
        Auth::guard('admin')->logout();
        return redirect()->route('admin.login')->with('success', 'Admin Logout successfully');
    }
}
