@extends('admin.layouts.admin') 
@section('content')
<div class="content-wrapper">
    <div class="tab-content tab-content-basic">
        <div class="tab-pane fade show active" id="overview" role="tabpanel" aria-labelledby="overview">
            <!-- Breadcrumbs -->
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('admin.index') }}">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="{{ route('admincategory.index') }}">Categories</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Create Category</li>
                </ol>
            </nav>
            <!-- End Breadcrumbs -->

            <h2>Create Category</h2>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <form method="POST" action="{{ route('admin.category.categorystore') }}" enctype="multipart/form-data">
                            @csrf
                            <!-- Name -->
                            <div class="row">

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="name">Name</label>
                                        <input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}" required>
                                        @error('name')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>

                                </div>
                           
                            <!-- Slug -->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="slug">Slug</label>
                                    <input type="text" class="form-control" id="slug" name="slug" value="{{ old('slug') }}" required>
                                    @error('slug')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                         
                            <!-- Description -->
                           <div class="col-md-6">
                            <div class="form-group">
                                <label for="description">Description</label>
                                <textarea class="form-control" id="description" name="description" required>{{ old('description') }}</textarea>
                                @error('description')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                           </div>

                           <div class="col-md-6">
                            <div class="form-group">
                                <label for="status">Status</label>
                                <select class="form-control" id="status" name="status">
                                    <option value="1">Active</option>
                                    <option value="0">Inactive</option>
                                </select>
                            </div>
                           </div>
                           <div class="col-md-6">
                            <div class="form-group">
                                <label for="parent_id">Parent Category</label>
                                <select class="form-control" id="parent_id" name="parent_id">
                                    <option value="">Select Parent Category</option>
                                    @foreach ($topCategories as $category)
                                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                           </div>
                           
                            <!-- Image Upload -->
                            <div class="form-group">
                                <label for="image">Image</label>
                                <input type="file" class="form-control-file" id="image" name="image" onchange="previewImage(event)">
                                @error('image')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                                <!-- Image Preview -->
                                <img id="image_preview" src="#" alt="Image Preview" style="display: none; max-width: 100%; margin-top: 10px;">
                            </div>
                            </div>
                          
                          
                            <button type="submit" class="btn btn-primary">Create Category</button>
                        </form>
                    </div>
                </div>
            
        </div>
    </div>
</div>

<script>
    function previewImage(event) {
        var reader = new FileReader();
        reader.onload = function(){
            var output = document.getElementById('image_preview');
            output.src = reader.result;
            output.style.display = 'block'; 
        }
        reader.readAsDataURL(event.target.files[0]);
    }

    // Auto-generate slug based on the name
    document.getElementById('name').addEventListener('input', function() {
        var name = this.value.trim().toLowerCase().replace(/[^a-z0-9]+/g, '-');
        document.getElementById('slug').value = name;
    });
</script>

@endsection
