<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Makeittrend </title>
  <link rel="stylesheet" href="{{asset('admin/assets/vendors/feather/feather.css')}}">
  <link rel="stylesheet" href="{{asset('admin/assets/vendors/mdi/css/materialdesignicons.min.css')}}">
  <link rel="stylesheet" href="{{asset('admin/assets/vendors/ti-icons/css/themify-icons.css')}}">
  <link rel="stylesheet" href="{{asset('admin/assets/vendors/typicons/typicons.css')}}">
  <link rel="stylesheet" href="{{asset('admin/assets/vendors/simple-line-icons/css/simple-line-icons.css')}}">
  <link rel="stylesheet" href="{{asset('admin/assets/vendors/css/vendor.bundle.base.css')}}">
  <link rel="stylesheet" href="{{asset('admin/assets/vendors/datatables.net-bs4/dataTables.bootstrap4.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('admin/assets/js/select.dataTables.min.css')}}">
  <link rel="stylesheet" href="{{asset('admin/assets/css/vertical-layout-light/style.css')}}">
  <link rel="shortcut icon" href="{{asset('admin/assets/images/favicon.png')}}" />
  <link rel ="stylesheet" href="{{asset('admin/assets/css/admin.css')}}" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">

  @if (Storage::disk('public')->exists('custom.css'))
<link rel="stylesheet" href="{{ asset('storage/custom.css') }}">
@endif
@stack('custom-css')

  
</head>

<body class="with-welcome-text">
    <div class="container-scroller">
        
        @include('admin.includes.navbar')
        
        <div class="container-fluid page-body-wrapper">
            <div class="theme-setting-wrapper"></div>
            
            
            @include('admin.includes.sidebar')    
      <div class="main-panel">
        @yield('content')
        <!-- content-wrapper ends -->
     @include('admin.includes.footer')
      </div>
    </div>
  </div>

  <script src="{{asset('admin/assets/vendors/js/vendor.bundle.base.js')}}"></script>
  <script src="{{asset('admin/assets/vendors/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>
  <script src="{{asset('admin/assets/vendors/chart.js/Chart.min.js')}}"></script>
  <script src="{{asset('admin/assets/vendors/progressbar.js/progressbar.min.js')}}"></script>
  <script src="{{asset('admin/assets/js/off-canvas.js')}}"></script>
  <script src="{{asset('admin/assets/js/hoverable-collapse.js')}}"></script>
  <script src="{{asset('admin/assets/js/template.js')}}"></script>
  <script src="{{asset('admin/assets/js/settings.js')}}"></script>
  <script src="{{asset('admin/assets/js/todolist.js')}}"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

  <!-- endinject -->
  <!-- Custom js for this page-->
  <script src="{{asset('admin/assets/js/jquery.cookie.js')}}" type="text/javascript"></script>
  <script src="{{asset('admin/assets/js/dashboard.js')}}"></script>
  <script src ="{{asset('admin/assets/js/admin.js')}}"></script>
  {{-- <script src="{{asset('admin/assets/js/proBanner.js')}}"></script> --}}
  <!-- <script src="../../assets/js/Chart.roundedBarCharts.js"></script> -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
  <!-- Ensure jQuery is loaded before your script -->
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js"></script>

  <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js"></script>

<script>
    $(document).ready(function() {
        // Check if there is a success or error message in the session
        @if(session('success'))
            toastr.success('{{ session('success') }}');
        @elseif(session('error'))
            toastr.error('{{ session('error') }}');
        @endif
    });
</script>


  
</body>

</html>