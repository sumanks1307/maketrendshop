@extends('admin.layouts.admin')
@section('content')

<div class="content-wrapper">
    <div class="tab-content tab-content-basic">
        <div class="tab-pane fade show active" id="overview" role="tabpanel" aria-labelledby="overview">
            <!-- Breadcrumbs -->
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('admin.index') }}">Dashboard</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Products</li>
                </ol>
            </nav>
            <!-- End Breadcrumbs -->
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="d-flex justify-content-between align-items-center mb-2">
                            <h2>Products</h2>

                        <a href="{{ route('admin.product.create') }}" class="btn btn-success mb-2">Create Product</a>
                        </div>
                        <table class="table table-bordered table-hover dt-responsive">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Slug</th>
                                    <th>Description</th>
                                    <th>Image</th>
                                    <th>Regular Price</th>
                                    <th>Sale Price</th>
                                    <th>Stock Status</th>
                                    <th>Quantity</th>
                                    <th>Featured</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($products as $product)
                                <tr>
                                    <td>{{ $product->id }}</td>
                                    <td>{{ $product->name }} <br> <br><b>SKU: </b> {{$product->SKU}}</td>
                                    <td>{{ $product->slug }}</td>
                                    <td>{{ $product->description }}</td>
                                    <td>
                                        @if($product->image)
                                            <img src="{{ url('storage/' . $product->image) }}" alt="{{ $product->name }}" style="width: 100px; height: auto;">
                                        @else
                                            <p>No image</p>
                                        @endif
                                    </td>
                                    <td>{{ $product->regular_price }}</td>
                                    <td>{{ $product->sale_price ?? '-' }}</td>
                                    <td>{{ $product->stock_status }}</td>
                                    <td>{{ $product->quantity }}</td>
                                    <td>{{ $product->featured ? 'Yes' : 'No' }}</td>
                                    <td>
                                        <a href="{{ route('admin.product.edit', ['id' => $product->id]) }}" class="btn btn-primary btn-sm">Edit</a>
                                        <form action="{{ route('admin.product.delete', ['id' => $product->id]) }}" method="POST" style="display: inline;">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
